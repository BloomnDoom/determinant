object Main
{
  def main(args:Array[String])=
  {
    if(args.length!=1) println("USAGE: R1C1,R1C2...,R2C1,R2C2...");
    else
    {
      val len=args(0).split(',').map(_.toString.toDouble).length;
      val dim=scala.math.pow(len,0.5);
      if(dim!=dim.toInt) println("Input must be a square matrix");
      else
      {
        val matrix=args(0).split(',').map(_.toString.toDouble).grouped(dim.toInt).toArray;
        println(determinant(matrix));
      }
    }
  }
  def determinant(mat:Array[Array[Double]]):Double=
  {
    mat match
    {
      case Array(Array(x))=>x;
      case Array(Array(a,b),Array(c,d))=>a*d-b*c;
      case _=>mat(0).zipWithIndex.map{case(elem,ind)=>
        elem*determinant(minor(ind,mat))}
          .zipWithIndex.map{case(elem,ind)=>elem*scala.math.pow(-1,ind)}
          .reduce(_+_);
    }
  }
  def minor(col:Int,mat:Array[Array[Double]]):Array[Array[Double]]=
  {
    //Remove first row as none of those elements will be in the minor
    val arr=mat.slice(1,mat.length);
    val min=arr.map(_.zipWithIndex.filter{case(elem,ind)=>ind!=col}
      .map(_._1));
    min;
  }
}
