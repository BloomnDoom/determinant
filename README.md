# Determinant Calculator

A program to find the determinant of a given square matrix.The determinant of a `1x1` matrix is the element itself. The determinant of a `2x2` matrix

`
|a  b|`<br>
`|c  d|`

is `a x d - b x c`.

The determinant of a `NxN` matrix is found by reducing it to a matrix of size `(N-1)x(N-1)`

This can be done by alternate addition and subtraction of the product of each element of the first row and its minor. The minor of an element is the matrix that is formed by removing the row and column where the element is found.

